<?php
declare(strict_types=1);

namespace App\Application\Output;

class RequestIsCreatedDto
{
    public int $id;
    public bool $status;

}