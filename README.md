# Описание REST Api приложения по работе с запросами
___
##  Запуск программы:
1. Создайте файл .env c переменными по примеру файла .env-example для работы с БД(пример):
2. Запустить docker:
```
docker-compose up
```
3. В программе Postman проверить работу запросов, предложенных ниже
4. Для отработки очереди запросов запустить в php контейнере в корне проекта cron - программу:
```
php cron.php
```
##  REST API запросы:
1. Создание запроса:
+ POST /api/v1/requests
+ Фомат запроса:
```
  {
  "firstname":"Алина",
  "email":"mail@mail.ru",
  "phone":"9111111111", //без 8ки
  "date1":"12.03.2010",
  "date2":"15.03.2010"
  }
 ```
2. Вывод всех запросов: 
+ GET /api/v1/requests
+ Пример ответа:
```
  {
  "firstname":"Алина",
  "email":"mail@mail.ru",
  "phone":"9111111111", //без 8ки
  "date1":"12.03.2010",
  "date2":"15.03.2010"
  },...
 ```
3. Вывод статуса выполнения запроса по id:
+ GET /api/v1/requests/{id}
+ Пример ответа:
```
  {
   status: false
  },...
 ```
+ После добавления запроса автаматически проставляется статус false. 
Запрос добавляется в очередь RabbitMQ.
После исполнения запроса, статус меняется на true.
Для просмотра выполнения запроса в тестовом варианте установлен sleep(10);